# IlhaSoft Support Validation Library

[![Release](https://jitpack.io/v/org.bitbucket.ilhasoft/support-validation.svg?style=flat-square)](https://jitpack.io/#org.bitbucket.ilhasoft/support-validation)

Biblioteca com objetivo de facilitar e agilizar a criação de formulários. 

## Recursos disponíveis:

* Configuração tamanho mínimo/máximo para campos de texto;
* Validar inputs baseado no tipo de dados utilizado (email, cartão de crédito, URL e etc);
* Mensagens pré-definidas traduzidas em vários idiomas;
* Suporte à [`TextInputLayout`](https://developer.android.com/reference/android/support/design/widget/TextInputLayout.html);

## Exemplo de utilização

![Exemplo de utilização](http://s32.postimg.org/7tfrxtd9x/device_2016_05_15_105341.png)

## Como utilizar?

[Documentação oficial](https://bitbucket.org/ilhasoft/support-validation/wiki/Como%20utilizar)
